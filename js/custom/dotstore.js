(function($){
$(document).ready(function(){

$('.collection-items').slick({
    dots: false,
    infinite: false,
    speed: 300,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  });
  $('.product-slider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });
  $('.features-slider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });

  $('.testimonial-slider').slick({
    dots: true,
    infinite: false,
    speed: 300,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });

  new WOW().init();

  $(".mobile-title").click(function(){
      $('.mobile-title').removeClass('active');
      $('.footer-links').hide();
      $(this).toggleClass('active').next('.footer-links').slideToggle();
  });
  $('.tab-nav-item').click(function(){  
    $(".tab-pane").removeClass('active');
    $(".tab-pane[data-id='"+$(this).attr('data-id')+"']").addClass("active");
    $(".tab-nav-item").removeClass('active');
    $(".tab-nav-item[data-id='"+$(this).attr('data-id')+"']").addClass("active");
  });
});

Drupal.behaviors.addAccessionBehavior = {
    attach: function(context, settings){
$(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});
  $('.add').click(function(){
    if ($(this).prev().val() < 999) {
    	$(this).prev().val(+$(this).prev().val() + 1);
		}
  });
  $('.sub').click(function(){
    if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});     
    }
  };

})(jQuery);

