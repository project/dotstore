<?php

/**
 * @file
 * Post update functions for dotstore.
 */

/**
 * Sets the default `base_primary_color` value of dotstore's theme settings.
 */
function dotstore_post_update_add_dotstore_primary_color() {
  \Drupal::configFactory()->getEditable('dotstore.settings')
    ->set('base_primary_color', '#1b9ae4')
    ->save();
}
