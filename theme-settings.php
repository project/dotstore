<?php

/**
 * @file
 * Functions to support dotstore theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for system_theme_settings.
 */
function dotstore_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['#validate'][] = 'dotstore_theme_settings_validate';
  $form['#attached']['library'][] = 'dotstore/color-picker';

   
  
  $form['dotstore_settings']['dotstore_head'] = [
    '#type' => 'details',
    '#title' => t('<b>Header Section</b>'),
    '#open' => TRUE,
  ];

  $form['dotstore_settings']['dotstore_head']['offer_message'] = [
    '#type' => 'textfield',
    '#title' => t('Offer Box'),
    '#default_value' => theme_get_setting('offer_message'),
  ];

  $form['dotstore_settings']['dotstore_head']['message_box'] = [
    '#type' => 'textfield',
    '#title' => t('Message Box'),
    '#default_value' => theme_get_setting('message_box'),
  ];

  

  $form['dotstore_settings']['dotstore_share'] = [
    '#type' => 'details',
    '#title' => t('<b>Social Share Section</b>'),
    '#open' => TRUE,
  ];
  
  $form['dotstore_settings']['dotstore_share']['facebbok_link'] = [
    '#type' => 'textfield',
    '#title' => t('Facebbok Link'),
    '#default_value' => theme_get_setting('facebbok_link'),
  ];
  
  $form['dotstore_settings']['dotstore_share']['twitter_link'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter Link'),
    '#default_value' => theme_get_setting('twitter_link'),
  ];

  $form['dotstore_settings']['dotstore_share']['instagram_link'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram Link'),
    '#default_value' => theme_get_setting('instagram_link'),
  ];
  
  $form['dotstore_settings']['dotstore_share']['youtube_link'] = [
    '#type' => 'textfield',
    '#title' => t('Youtube Link'),
    '#default_value' => theme_get_setting('youtube_link'),
  ];



  $form['dotstore_settings']['dotstore_footer'] = [
    '#type' => 'details',
    '#title' => t('<b>Footer Section</b>'),
    '#open' => TRUE,
  ];

  $form['dotstore_settings']['dotstore_footer']['contact_us'] = [
    '#type' => 'textarea',
    '#title' => t('Contact Us'),
    '#default_value' => theme_get_setting('contact_us'),
  ];

  $form['dotstore_settings']['dotstore_footer']['phone_number'] = [
    '#type' => 'textfield',
    '#title' => t('Phone Number'),
    '#default_value' => theme_get_setting('phone_number'),
  ];

  $form['dotstore_settings']['dotstore_footer']['email_address'] = [
    '#type' => 'email',
    '#title' => t('Email Address'),
    '#default_value' => theme_get_setting('email_address'),
  ];

  $form['dotstore_settings']['dotstore_footer']['copy_right'] = [
    '#type' => 'textfield',
    '#title' => t('Copyright Section'),
    '#default_value' => theme_get_setting('copy_right'),
  ];



  $color_config = [
    'colors' => [
      'base_primary_color' => 'Primary base color',
    ],
    'schemes' => [
      'default' => [
        'label' => 'Blue Lagoon',
        'colors' => [
          'base_primary_color' => '#1b9ae4',
        ],
      ],
      'firehouse' => [
        'label' => 'Firehouse',
        'colors' => [
          'base_primary_color' => '#a30f0f',
        ],
      ],
      'ice' => [
        'label' => 'Ice',
        'colors' => [
          'base_primary_color' => '#57919e',
        ],
      ],
      'plum' => [
        'label' => 'Plum',
        'colors' => [
          'base_primary_color' => '#7a4587',
        ],
      ],
      'slate' => [
        'label' => 'Slate',
        'colors' => [
          'base_primary_color' => '#47625b',
        ],
      ],
    ],
  ];
  
  $form['#attached']['drupalSettings']['dotstore']['colorSchemes'] = $color_config['schemes'];

  $form['dotstore_settings']['dotstore_utilities'] = [
    '#type' => 'fieldset',
    '#title' => t('dotstore Utilities'),
  ];
  $form['dotstore_settings']['dotstore_utilities']['mobile_menu_all_widths'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable mobile menu at all widths'),
    '#default_value' => theme_get_setting('mobile_menu_all_widths'),
    '#description' => t('Enables the mobile menu toggle at all widths.'),
  ];
  $form['dotstore_settings']['dotstore_utilities']['site_branding_bg_color'] = [
    '#type' => 'select',
    '#title' => t('Header site branding background color'),
    '#options' => [
      'default' => t('Primary Branding Color'),
      'gray' => t('Gray'),
      'white' => t('White'),
    ],
    '#default_value' => theme_get_setting('site_branding_bg_color'),
  ];
  $form['dotstore_settings']['dotstore_utilities']['dotstore_color_scheme'] = [
    '#type' => 'fieldset',
    '#title' => t('dotstore Color Scheme Settings'),
  ];
  $form['dotstore_settings']['dotstore_utilities']['dotstore_color_scheme']['description'] = [
    '#type' => 'html_tag',
    '#tag' => 'p',
    '#value' => t('These settings adjust the look and feel of the dotstore theme. Changing the color below will change the base hue, saturation, and lightness values the dotstore theme uses to determine its internal colors.'),
  ];
  $form['dotstore_settings']['dotstore_utilities']['dotstore_color_scheme']['color_scheme'] = [
    '#type' => 'select',
    '#title' => t('dotstore Color Scheme'),
    '#empty_option' => t('Custom'),
    '#empty_value' => '',
    '#options' => [
      'default' => t('Blue Lagoon (Default)'),
      'firehouse' => t('Firehouse'),
      'ice' => t('Ice'),
      'plum' => t('Plum'),
      'slate' => t('Slate'),
    ],
    '#input' => FALSE,
    '#wrapper_attributes' => [
      'style' => 'display:none;',
    ],
  ];

  foreach ($color_config['colors'] as $key => $title) {
    $form['dotstore_settings']['dotstore_utilities']['dotstore_color_scheme'][$key] = [
      '#type' => 'textfield',
      '#maxlength' => 7,
      '#size' => 10,
      '#title' => t($title),
      '#description' => t('Enter color in full hexadecimal format (#abc123).') . '<br/>' . t('Derivatives will be formed from this color.'),
      '#default_value' => theme_get_setting($key),
      '#attributes' => [
        'pattern' => '^#[a-fA-F0-9]{6}',
      ],
      '#wrapper_attributes' => [
        'data-drupal-selector' => 'dotstore-color-picker',
      ],
    ];
  }
}

/**
 * Validation handler for the dotstore system_theme_settings form.
 */
function dotstore_theme_settings_validate($form, FormStateInterface $form_state) {
  if (!preg_match('/^#[a-fA-F0-9]{6}$/', $form_state->getValue('base_primary_color'))) {
    $form_state->setErrorByName('base_primary_color', t('Colors must be 7-character string specifying a color hexadecimal format.'));
  }
}
